﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Companyname.TransportLayer.Providers
{
    public class SFTPProvider : IProviderInterface
    {

        Chilkat.SFtp sftpObject = new Chilkat.SFtp();
        Chilkat.SFtpDir sftpDirObject = new Chilkat.SFtpDir();
        Chilkat.SFtpFile sftpFileObject = new Chilkat.SFtpFile();

        private string localDownloadPath;
        private string localUploadPath;
        private bool localFoldersExist;


        public SFTPProvider(string hostName, 
                                   string userName, 
                                   string password, 
                                   int port, 
                                   string unlockComponent, 
                                   string localDownloadPath, 
                                   string localUploadPath)
        {
            sftpObject.UnlockComponent(unlockComponent);
            bool isConnected = sftpObject.Connect(hostName, port);
            bool isAuthenticated = sftpObject.AuthenticatePw(userName, password);
            bool isInitialized = sftpObject.InitializeSftp();

            this.localDownloadPath = localDownloadPath;
            this.localUploadPath = localUploadPath;

            if (!this.localFoldersExist)
            {
                Directory.CreateDirectory(localDownloadPath);
                Directory.CreateDirectory(localUploadPath);
                this.localFoldersExist = true;
            }
        }

        public void DeleteFile(string filePath)
        {
            sftpObject.RemoveFile(filePath);
        }

        public void MoveFile(string fromFilePath, string toFilePath)
        {
            string handle = sftpObject.OpenFile(toFilePath, "writeOnly", "createTruncate");
            sftpObject.UploadFile(handle, fromFilePath);
            sftpObject.CloseHandle(handle);
            File.Delete(fromFilePath);
        }

        public void SaveToFile(string filePath, string contents)
        {
            File.WriteAllText(localUploadPath + Path.GetFileName(filePath), contents, Encoding.UTF8);
            string handle = sftpObject.OpenFile(filePath, "writeOnly", "createTruncate");
            sftpObject.UploadFile(handle, localUploadPath + Path.GetFileName(filePath));
            sftpObject.CloseHandle(handle);
            File.Delete(localUploadPath + Path.GetFileName(filePath));
        }

        public FileStream GetFile(string filePath)
        {
            string localPath = localDownloadPath + Path.GetFileName(filePath);
            string handle = sftpObject.OpenFile(filePath, "readOnly", "openExisting");
            sftpObject.DownloadFile(handle, localPath);
            sftpObject.RemoveFile(filePath);

            return new FileStream(localPath, FileMode.Open, System.IO.FileAccess.ReadWrite);
        }

        public string[] ListDirectory(string path)
        {
            var directoryPath = new List<string>();

            string handle = sftpObject.OpenDir(path);
            sftpDirObject = sftpObject.ReadDir(handle);
            sftpObject.CloseHandle(handle);

            for(int i = 0; i<sftpDirObject.NumFilesAndDirs; i++)
            {
                directoryPath.Add(path + sftpDirObject.GetFilename(i));
            }

            return directoryPath.ToArray();
        }

        public string GetName()
        {
            return "SFTP";
        }

        public string CombinePath(string fileLocation, string fileName)
        {
            return fileLocation + fileName;
        }

    }
}
