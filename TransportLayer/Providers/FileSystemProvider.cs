﻿using System.IO;
using System.Text;

namespace Companyname.TransportLayer.Providers
{
    public class FileSystemProvider : IProviderInterface
    {
        public void DeleteFile(string filePath)
        {
            File.Delete(filePath);
        }

        public FileStream GetFile(string filePath)
        {
            return new FileStream(filePath, FileMode.Open);
        }

        public void MoveFile(string fromFilePath, string toFilePath)
        {
            File.Move(fromFilePath, toFilePath);
        }

        public void SaveToFile(string filePath, string contents)
        {
            File.WriteAllText(filePath, contents, Encoding.UTF8);
        }

        public string[] ListDirectory(string path)
        {
            return Directory.GetFiles(path);
        }

        public string GetName()
        {
            return "FileSystem";
        }

        public string CombinePath(string fileLocation, string fileName)
        {
            return Path.Combine(fileLocation, fileName);
        }
    }
}
