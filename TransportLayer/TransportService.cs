﻿using System.IO;

namespace Companyname.TransportLayer
{
    public class TransportService
    {
        private IProviderInterface provider;

        public TransportService(IProviderInterface provider)
        {
            this.provider = provider;
        }


        public FileStream GetFile(string filePath)
        {
            return this.provider.GetFile(filePath);    
        }

        public void SaveToFile(string filePath, string contents)
        {
            this.provider.SaveToFile(filePath, contents);
        }

        public void MoveFile(string fromFilePath, string toFilePath)
        {
            this.provider.MoveFile(fromFilePath, toFilePath);
        }

        public void DeleteFile(string filePath)
        {
            this.provider.DeleteFile(filePath);
        }

        public string[] ListDirectory(string path)
        {
            return this.provider.ListDirectory(path);
        }

        public string GetProviderName()
        {
            return this.provider.GetName();
        }

        public string GetCombinedPath(string fileLocation, string fileName)
        {
            return this.provider.CombinePath(fileLocation, fileName);
        }

    }
}
