﻿using System.IO;

namespace Companyname.TransportLayer
{
    public interface IProviderInterface
    {
        FileStream GetFile(string filePath);

        void SaveToFile(string filePath, string contents);

        void MoveFile(string fromFilePath, string toFilePath);

        void DeleteFile(string filePath);

        string[] ListDirectory(string path);

        string GetName();

        string CombinePath(string fileLocation, string fileName);
    }
}
