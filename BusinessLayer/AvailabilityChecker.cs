﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Companyname.CNCRouter.Feeds;

namespace Companyname.CNCRouter.BusinessLayer
{
    class AvailabilityChecker
    {
        private string storeCode;

        private string warehouseCode;

        private List<LineItemInfo> items;

        private DataTable resultDataTable;

        private Dictionary<string, int> storeAvailability;
        private Dictionary<string, int> warehouseAvailability;

        public AvailabilityChecker(List<LineItemInfo> items, string storeCode, string warehouseCode)
        {
            this.storeCode = storeCode;
            this.warehouseCode = warehouseCode;
            this.items = items;
            this.resultDataTable = new DataTable();
            this.storeAvailability = new Dictionary<string, int>();
            this.warehouseAvailability = new Dictionary<string, int>();
            this.FetchAvailableItemInformation();
            this.MapFetchedDataToAvailabilityMap();
        }

        public List<LineItemInfo> GetAvailableItemsInStore()
        {
            List<LineItemInfo> availableItems = new List<LineItemInfo>();
            foreach (LineItemInfo item in items)
            {
                if (this.storeAvailability[item.SKU] >= int.Parse(item.Quantity))
                {
                    availableItems.Add(item);
                }
            }
            return availableItems;
        }

        public List<LineItemInfo> GetAvailableItemsInWarehouse()
        {
            List<LineItemInfo> availableItems = new List<LineItemInfo>();
            foreach (LineItemInfo item in items)
            {
                //Check here externally
                if (this.warehouseAvailability[item.SKU] >= int.Parse(item.Quantity))
                {
                    availableItems.Add(item);
                }
            }
            return availableItems;
        }

        public List<LineItemInfo> GetCombinedAvailability(List<LineItemInfo> availableAtStore, List<LineItemInfo> availableAtWarehouse)
        {
            return availableAtStore.Union(availableAtWarehouse).ToList();
        }

        private void FetchAvailableItemInformation()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["AvailabilityConnectionString"].ConnectionString;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                string queryString = @"DECLARE @TempTable custom_ItemSKUNOs, 
                                               @SKU varchar(30), 
                                               @VendorNo varchar(30), 
                                               @WarehouseVendorNo varchar(30);
                                           SET @VendorNo = @VendorNoValue;
                                           SET @WarehouseVendorNo = @WarehouseVendorNoValue;";
                foreach (var item in this.items)
                {
                    queryString += string.Format(@"INSERT INTO @TempTable(SKUNo) values ('{0}');", item.SKU) + Environment.NewLine;
                }

                queryString += @"EXEC custom_VendorItemInventorySelect 
                                @VendorNo = @VendorNo, 
                                @WarehouseVendorNo = @WarehouseVendorNo, 
                                @Items = @TempTable";


                using (SqlCommand command = new SqlCommand(queryString, conn))
                {
                    command.Parameters.AddWithValue("@VendorNoValue", this.storeCode);
                    command.Parameters.AddWithValue("@WarehouseVendorNoValue", this.warehouseCode);

                    using (SqlDataAdapter dataAdapter = new SqlDataAdapter(command))
                    {
                        conn.Open();
                        dataAdapter.Fill(this.resultDataTable);
                    }
                }
            }
        }

        private void MapFetchedDataToAvailabilityMap()
        {
            foreach (DataRow row in this.resultDataTable.Rows)
            {
                string sku = row["SKUNo"].ToString().Trim();
                string vendorNo = row["VendorNo"].ToString().Trim();
                float quantity;
                if (!float.TryParse(row["quantity"].ToString().Trim(), out quantity))
                {
                    quantity = 0;
                }

                if (vendorNo == this.storeCode)
                {
                    this.storeAvailability.Add(sku, (int)quantity);
                } else if (vendorNo == this.warehouseCode)
                {
                    this.warehouseAvailability.Add(sku, (int)quantity);
                }
            }
        }
    }
}
