﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Companyname.CNCRouter.BusinessLayer
{
    public class ApplicationSettings
    {
        public NameValueCollection incomingFileLocations { get; } = (NameValueCollection)ConfigurationManager.GetSection("CNCRouterApplicationSettings/IncomingFileLocations");
        public NameValueCollection outgoingFileLocations { get; } = (NameValueCollection)ConfigurationManager.GetSection("CNCRouterApplicationSettings/OutgoingFileLocations");
        public NameValueCollection archiveFileLocations { get; } = (NameValueCollection)ConfigurationManager.GetSection("CNCRouterApplicationSettings/ArchiveFileLocations");
        public NameValueCollection incomingFilenameMasks { get; } = (NameValueCollection)ConfigurationManager.GetSection("CNCRouterApplicationSettings/IncomingFilenameMasks");
        public NameValueCollection supportedFeedTypes { get; } = (NameValueCollection)ConfigurationManager.GetSection("CNCRouterApplicationSettings/SupportedFeedTypes");
        public NameValueCollection LoggingFileConfiguration { get; } = (NameValueCollection)ConfigurationManager.GetSection("CNCRouterApplicationSettings/NLogFileName");
        public NameValueCollection SFTPConfiguration { get; } = (NameValueCollection)ConfigurationManager.GetSection("CNCRouterApplicationSettings/SFTPConfiguration");
        public NameValueCollection TransportLayer { get; } = (NameValueCollection)ConfigurationManager.GetSection("CNCRouterApplicationSettings/TransportLayer");
    }
}
