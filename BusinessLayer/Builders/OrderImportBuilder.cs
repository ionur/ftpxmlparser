﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Companyname.CNCRouter.DataLayer;
using Companyname.CNCRouter.DataLayer.Models;
using Companyname.CNCRouter.Feeds;

namespace Companyname.CNCRouter.BusinessLayer.Builders
{
    public class OrderImportBuilder
    {
        public ImportOrders BuildSFSOrderForItems(ImportOrders originalImportOrdersObject, List<LineItemInfo> itemList = null, string storeCode = "DEFAULT")
        {
            originalImportOrdersObject.OrderInfo.OrderNumber = GetSFSOrderNumber(originalImportOrdersObject.OrderInfo.OrderNumber);
            originalImportOrdersObject.OrderInfo.HeaderInfo.ShipMethCode = "STANDARD";
            originalImportOrdersObject.OrderInfo.HeaderInfo.ShipMethod = "Standard";
            originalImportOrdersObject.OrderInfo.HeaderInfo.StoreCode = storeCode;

            originalImportOrdersObject.OrderInfo.LineItemInfo = itemList;
            return originalImportOrdersObject;
        }

        public ImportOrders BuildISPUOrderForItems(ImportOrders originalImportOrdersObject, List<LineItemInfo> itemList = null, string storeCode = null)
        {
            originalImportOrdersObject.OrderInfo.OrderNumber = GetISPUOrderNumber(originalImportOrdersObject.OrderInfo.OrderNumber);
            originalImportOrdersObject.OrderInfo.HeaderInfo.ShipMethCode = "ISPU";
            originalImportOrdersObject.OrderInfo.HeaderInfo.ShipMethod = "ISPU";
            originalImportOrdersObject.OrderInfo.HeaderInfo.StoreCode = "ISPU";
            originalImportOrdersObject.OrderInfo.HeaderInfo.EventId = "ISPU";
            originalImportOrdersObject.OrderInfo.HeaderInfo.AllocStoreCode = storeCode;

            originalImportOrdersObject.OrderInfo.LineItemInfo = itemList;
            return originalImportOrdersObject;
        }

        public string GetSFSOrderNumber(string originalOrderNumber)
        {
            int OutgoingSFSOrderCount = 0;
            using (var persistencyService = new PersistencyService())
            {
                OutgoingSFSOrderCount = persistencyService.GetOutgoingSFSOrderCount(originalOrderNumber);
            }
            OutgoingSFSOrderCount++; //The counter will always start from 1

            return "SF" + OutgoingSFSOrderCount + "_" + originalOrderNumber;
        }

        public string GetISPUOrderNumber(string originalOrderNumber)
        {
            return "ISPU_" + originalOrderNumber;
        }

        public OiIncomingSTS GetOriginalOiIncomingSTSEntity(string originalOrderNumber)
        {
            using (var persistency = new PersistencyService())
            {
                return persistency.GetOiIncomingSTSDetails(originalOrderNumber);
            }
        }

        public OiOutgoingSTS GetOriginalOiOutgoingSTSEntity(string originalOrderNumber)
        {
            using (var persistency = new PersistencyService())
            {
                return persistency.GetOiOutgoingSTSDetails(originalOrderNumber);
            }
        }

        public string StripOrderPrefixFromOrderNumber(string orderNumber, string prefixRegex)
        {
            return Regex.Replace(orderNumber, prefixRegex, String.Empty);
        }

        public string GetUnitPrice(List<LineItemInfo> lineItemInfoList, string lineNumber)
        {
            string unitPrice = "";
            foreach (LineItemInfo lineItemInfo in lineItemInfoList)
            {
                if (lineItemInfo.LineNumber == lineNumber)
                    unitPrice = lineItemInfo.UnitPrice;
            }

            return unitPrice;
        }
    }
}
