﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Companyname.CNCRouter.BusinessLayer.Processors;
using Companyname.CNCRouter.Feeds;
using Companyname.LoggerLayer;

namespace Companyname.CNCRouter.BusinessLayer
{
    public sealed class LogicService
    {
        private FileStream inputFile;

        private IProcessorInterface processor;

        private ApplicationSettings settings;

        private LoggerService logger;

        public string processId { get; }

        public void setLogger(LoggerService logger)
        {
            this.logger = logger;
        }

        public LogicService(FileStream file,
                     TransportLayer.TransportService transport,
                     FeedSerializer serializer,
                     ApplicationSettings settings,
                     string processId)
        {
            this.settings = settings;
            this.inputFile = file;
            this.processId = processId;           

            string feedType = GetFeedType(Path.GetFileName(file.Name));
            switch (feedType)
            {
                case "ShipmentUpdate":
                    processor = new ShipmentUpdateProcessor(transport, serializer, settings.outgoingFileLocations[feedType], settings.archiveFileLocations[feedType], processId, settings.outgoingFileLocations["OrderImport"]);
                    break;
                case "OrderImport":
                    processor = new OrderImportProcessor(transport, serializer, settings.outgoingFileLocations[feedType], settings.archiveFileLocations[feedType], processId);
                    break;
                case "CancelUpdate":
                    processor = new CancelUpdateProcessor(transport, serializer, settings.outgoingFileLocations[feedType], settings.archiveFileLocations[feedType], processId);
                    break;
                default:
                    throw new NotSupportedException(string.Format("Processing of Feed Type - {0} is not supported", feedType));
            }
        }

        public void Process()
        {
            var xmlObject = this.processor.GetXmlObject(this.inputFile);
            if (this.processor.IsPassThru(xmlObject))
            {
                this.processor.PassThruAction(this.inputFile);
                return;
            }
            else if (this.processor.ShouldManipulateFile(xmlObject))
            {
                this.processor.ManipulateAction(xmlObject, this.inputFile);
                return;
            }
        }

        private string GetFeedType(string filename)
        {
            foreach (string key in settings.incomingFilenameMasks)
            {
                string incomingFilenameMask = settings.incomingFilenameMasks[key];
                Regex r = new Regex(incomingFilenameMask);
                Match m = r.Match(filename);
                if (m.Success)
                {
                    return key;
                }
            }
            throw new NotSupportedException("Could not get the feed type of incoming feed");
        }
        
    }
}
