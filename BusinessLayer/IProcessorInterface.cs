﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Companyname.CNCRouter.Feeds;

namespace Companyname.CNCRouter.BusinessLayer
{
    interface IProcessorInterface
    {
        bool IsPassThru(CompanyNameFeed xmlObject);

        void PassThruAction(FileStream file);

        bool ShouldManipulateFile(CompanyNameFeed xmlObject);

        void ManipulateAction(CompanyNameFeed xmlObject, FileStream file);

        CompanyNameFeed GetXmlObject(FileStream file);
    }
}
