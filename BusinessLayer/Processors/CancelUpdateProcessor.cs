﻿using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Companyname.CNCRouter.BusinessLayer.Builders;
using Companyname.CNCRouter.Feeds;
using Companyname.TransportLayer;

namespace Companyname.CNCRouter.BusinessLayer.Processors
{
    class CancelUpdateProcessor : BaseProcessor, IProcessorInterface
    {
        private const string orderNumberSTSRegEx = "ST[0-9]+_";
        private const string orderNumberISPURegEx = "ISP[0-9]+_";
        private const string orderNumberSFSRegEx = "SF[0-9]+_";

        private OrderImportBuilder oiBuilder;

        public CancelUpdateProcessor(
            TransportService transport,
            FeedSerializer serializer,
            string outgoingFileLocation,
            string archiveFileLocation,
            string processId) : base(transport, serializer, outgoingFileLocation, archiveFileLocation, processId)
        {
            oiBuilder = new OrderImportBuilder();
        }

        public CompanyNameFeed GetXmlObject(FileStream file)
        {
            return serializer.Deserialize<CustomerServiceUpdate>(file);
        }

        public bool IsPassThru(CompanyNameFeed xmlObject)
        {
            //will be implemented in the next conditions, for the moment, it returns false
            return false;
        }

        public bool ShouldManipulateFile(CompanyNameFeed xmlObject)
        {
            CustomerServiceUpdate customerServiceUpdateObject = xmlObject as CustomerServiceUpdate;
            if (IsOrderNumberISP(customerServiceUpdateObject.CustomerService.Header.OrderNumber) ||
                    IsOrderNumberSFS(customerServiceUpdateObject.CustomerService.Header.OrderNumber) ||
                    IsOrderNumberSTS(customerServiceUpdateObject.CustomerService.Header.OrderNumber))
            {
                return true;
            }
            return false;
        }

        public void ManipulateAction(CompanyNameFeed xmlObject, FileStream file)
        {
            CustomerServiceUpdate customerServiceUpdateObject = xmlObject as CustomerServiceUpdate;
            if(IsOrderNumberISP(customerServiceUpdateObject.CustomerService.Header.OrderNumber))
            {
                ManipulateISPCancelUpdate(customerServiceUpdateObject, file);
            }
            if(IsOrderNumberSFS(customerServiceUpdateObject.CustomerService.Header.OrderNumber))
            {
                ManipulateSFSCancelUpdate(customerServiceUpdateObject, file);
            }
            if(IsOrderNumberSTS(customerServiceUpdateObject.CustomerService.Header.OrderNumber))
            {
                ManipulateSTSCancelUpdate(customerServiceUpdateObject, file);
            }            
            ArchiveOriginalFile(file);
        }

        private void ManipulateISPCancelUpdate(CustomerServiceUpdate customerServiceUpdateObject, FileStream file)
        {
            if (customerServiceUpdateObject.CustomerService.CancelInfo.ReasonCode == "AUTO")
            {
                //US #28
                string originalOrderNumber = oiBuilder.StripOrderPrefixFromOrderNumber(customerServiceUpdateObject.CustomerService.Header.OrderNumber, orderNumberISPURegEx);
                var originalIncomingSTSEntity = oiBuilder.GetOriginalOiIncomingSTSEntity(originalOrderNumber);
                ImportOrders originalImportOrdersObject = serializer.Deserialize<ImportOrders>(originalIncomingSTSEntity.FileContents);
                
                List<LineItemInfo> itemList = new List<LineItemInfo>();
                string originalStoreCode = originalImportOrdersObject.OrderInfo.HeaderInfo.StoreCode;
                string targetStoreCode = "DEFAULT";
                List<LineItemInfo> availableAtWarehouseItems = new List<LineItemInfo>();

                foreach (var cancelledLine in customerServiceUpdateObject.CustomerService.CancelProduct)
                {
                    //TODO : check availability checker

                    LineItemInfo orderLine = new LineItemInfo();
                    orderLine.SKU = cancelledLine.SKU;
                    orderLine.StoreSKU = cancelledLine.VendorSKU;
                    orderLine.Quantity = float.Parse(cancelledLine.CancelledQuantity).ToString();
                    orderLine.LineNumber = cancelledLine.LineNumber;
                    orderLine.UnitPrice = oiBuilder.GetUnitPrice(originalImportOrdersObject.OrderInfo.LineItemInfo, cancelledLine.LineNumber);

                    itemList.Add(orderLine);
                }

                //TODO : STORECODE will be changed after availability checker works

                AvailabilityChecker availabilityChecker = new AvailabilityChecker(
                    itemList,
                    originalStoreCode,
                    GetWarehouseCodeFromCountry(originalImportOrdersObject.OrderInfo.HeaderInfo.ContactInfo[1].AddressInfo.CountryCode)
                    );
                availableAtWarehouseItems = availabilityChecker.GetAvailableItemsInWarehouse();
                if (itemList.Count != availableAtWarehouseItems.Count)
                {
                    targetStoreCode = "CNCSPLIT";
                }
                ImportOrders sfsOrder = oiBuilder.BuildSFSOrderForItems(originalImportOrdersObject, itemList, targetStoreCode);
                TransportAndPersistOutgoingOrder(sfsOrder, "SFS", originalOrderNumber);
            }

            else
            {
                customerServiceUpdateObject.CustomerService.Header.OrderNumber = Regex.Replace(customerServiceUpdateObject.CustomerService.Header.OrderNumber, orderNumberISPURegEx, "");
                string customerServiceUpdatetoString = serializer.Serialize<CustomerServiceUpdate>(customerServiceUpdateObject);
                transport.SaveToFile(Path.Combine(outgoingFileLocation, Path.GetFileName(file.Name)), customerServiceUpdatetoString);
            }
        }        

        private void ManipulateSFSCancelUpdate(CustomerServiceUpdate customerServiceUpdateObject, FileStream file)
        {
            if (customerServiceUpdateObject.CustomerService.CancelInfo.ReasonCode == "INT_COL")
            {
                //US #25
                string originalOrderNumber = oiBuilder.StripOrderPrefixFromOrderNumber(
                    customerServiceUpdateObject.CustomerService.Header.OrderNumber,
                    orderNumberISPURegEx
                    );
                var originalIncomingSTSEntity = oiBuilder.GetOriginalOiIncomingSTSEntity(originalOrderNumber);
                ImportOrders originalImportOrdersObject = serializer.Deserialize<ImportOrders>(originalIncomingSTSEntity.FileContents);

                List<LineItemInfo> itemList = new List<LineItemInfo>();
                int lineNumber = 1;
                foreach(var cancelledLine in customerServiceUpdateObject.CustomerService.CancelProduct)
                {
                    LineItemInfo orderLine = new LineItemInfo();
                    orderLine.SKU = cancelledLine.SKU;
                    orderLine.StoreSKU = cancelledLine.VendorSKU;
                    orderLine.Quantity = float.Parse(cancelledLine.CancelledQuantity).ToString();
                    orderLine.LineNumber = lineNumber.ToString();                    
                    orderLine.UnitPrice = oiBuilder.GetUnitPrice(originalImportOrdersObject.OrderInfo.LineItemInfo, cancelledLine.LineNumber);

                    itemList.Add(orderLine);
                    lineNumber++;
                }

                //Create an ISPU order
                ImportOrders ispuOrder = oiBuilder.BuildISPUOrderForItems(originalImportOrdersObject, itemList, originalIncomingSTSEntity.VendorCode);
                TransportAndPersistOutgoingOrder(ispuOrder, "ISPU", originalOrderNumber);
            }
            else
            {
                customerServiceUpdateObject.CustomerService.Header.OrderNumber = Regex.Replace(customerServiceUpdateObject.CustomerService.Header.OrderNumber, orderNumberSFSRegEx, "");
                string customerServiceUpdatetoString = serializer.Serialize<CustomerServiceUpdate>(customerServiceUpdateObject);
                transport.SaveToFile(Path.Combine(outgoingFileLocation, Path.GetFileName(file.Name)), customerServiceUpdatetoString);
            }
        }        

        private void ManipulateSTSCancelUpdate(CustomerServiceUpdate customerServiceUpdateObject, FileStream file)
        {
            //connecting to db and manipulate 
            if (customerServiceUpdateObject.CustomerService.CancelInfo.ReasonCode == "INT_DAM")
            {
                string originalOrderNumber = oiBuilder.StripOrderPrefixFromOrderNumber(customerServiceUpdateObject.CustomerService.Header.OrderNumber, orderNumberSTSRegEx);
                var originalOutgoingSTSEntity = oiBuilder.GetOriginalOiOutgoingSTSEntity(originalOrderNumber);
                ImportOrders originalImportOrdersObject = serializer.Deserialize<ImportOrders>(originalOutgoingSTSEntity.OriginalSFSOrder.FileContents);

                originalImportOrdersObject.OrderInfo.HeaderInfo.StoreCode = "HOT_CNCDMG";
                TransportAndPersistOutgoingOrder(originalImportOrdersObject, "SFS", originalOrderNumber);
            }
        }


        private bool IsOrderNumberISP(string shipmentUpdateOrderNumber)
        {
            Regex r = new Regex(orderNumberISPURegEx);
            if (r.Match(shipmentUpdateOrderNumber).Success) 
            {
                return true;
            }
            return false;
        }

        private bool IsOrderNumberSFS(string shipmentUpdateOrderNumber)
        {
            Regex r = new Regex(orderNumberSFSRegEx);
            if (r.Match(shipmentUpdateOrderNumber).Success)
            {
                return true;
            }
            return false;
        }

        private bool IsOrderNumberSTS(string shipmentUpdateOrderNumber)
        {
            Regex r = new Regex(orderNumberSTSRegEx);
            if (r.Match(shipmentUpdateOrderNumber).Success)
            {
                return true;
            }
            return false;
        }


    }
}
