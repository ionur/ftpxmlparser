﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Companyname.TransportLayer;
using Companyname.CNCRouter;
using Companyname.CNCRouter.Feeds;
using Companyname.CNCRouter.DataLayer;

namespace Companyname.CNCRouter.BusinessLayer.Processors
{
    abstract class BaseProcessor
    {
        protected TransportService transport;
        protected FeedSerializer serializer;

        protected string outgoingFileLocation;
        protected string archiveFileLocation;

        protected string processId;


        public BaseProcessor(TransportService transport, FeedSerializer serializer, string outgoingFileLocation, string archiveFileLocation, string processId)
        {
            this.transport = transport;
            this.serializer = serializer;
            this.outgoingFileLocation = outgoingFileLocation;
            this.archiveFileLocation = archiveFileLocation;
            this.processId = processId;
        }

        public void PassThruAction(FileStream file)
        {
            string fileName = Path.GetFileName(file.Name);
            string destinationFile = transport.GetCombinedPath(outgoingFileLocation, fileName);
            transport.MoveFile(file.Name, destinationFile);
        }

        protected void ArchiveOriginalFile(FileStream file)
        {
            string destinationFile = transport.GetCombinedPath(archiveFileLocation, Path.GetFileName(file.Name));
            transport.MoveFile(file.Name, destinationFile);
        }

        protected void TransportAndPersistOutgoingOrder(ImportOrders outgoingOrder, string orderType, string originalOrderNumber)
        {
            string writeFileName = outgoingOrder.OrderInfo.OrderNumber.ToString() + ".xml";
            string writeFilePath = Path.Combine(outgoingFileLocation, writeFileName);
            string fileContents = serializer.Serialize<ImportOrders>(outgoingOrder);
            transport.SaveToFile(writeFilePath, fileContents);

            using (var persistencyService = new PersistencyService())
            {
                if (orderType == "SFS")
                {
                    persistencyService.RegisterOutgoingSFSOrderImport(
                        outgoingOrder.OrderInfo.OrderNumber,
                        writeFileName,
                        fileContents,
                        originalOrderNumber,
                        processId);
                }
                else if (orderType == "ISPU")
                {
                    persistencyService.RegisterOutgoingISPUOrderImport(
                        outgoingOrder.OrderInfo.OrderNumber,
                        writeFileName,
                        fileContents,
                        originalOrderNumber, 
                        processId);
                }

            }
        }

        protected string GetWarehouseCodeFromCountry(string countryCode)
        {
            switch (countryCode)
            {
                case "SG":
                    return "SGWH";
                case "PH":
                    return "PHWH";
                case "MY":
                    return "MYWH";
                case "TH":
                    return "THPH";
                default:
                    return "WHS001";
            }
        }
    }
}
