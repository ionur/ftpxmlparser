﻿using System.IO;
using System.Text.RegularExpressions;
using Companyname.CNCRouter.DataLayer;
using Companyname.CNCRouter.DataLayer.Models;
using Companyname.CNCRouter.Feeds;
using Companyname.TransportLayer;

namespace Companyname.CNCRouter.BusinessLayer.Processors
{
    class ShipmentUpdateProcessor : BaseProcessor, IProcessorInterface
    {
        private const string orderNumberSTSRegEx = "ST[0-9]+_";
        private const string orderNumberSFSRegEx = "SF[0-9]+_";
        private const string orderNumberISPURegEx = "ISP[0-9]+_";

        private string outgoingOiFileLocation;

        public ShipmentUpdateProcessor(
            TransportService transport,
            FeedSerializer serializer,
            string outgoingFileLocation,
            string archiveFileLocation,
            string processId,
            string outgoingOiFileLocation = null) : base(transport, serializer, outgoingFileLocation, archiveFileLocation, processId)
        {
            if (outgoingOiFileLocation == null)
            {
                outgoingOiFileLocation = outgoingFileLocation;
            }

            this.outgoingOiFileLocation = outgoingOiFileLocation;
        }

        public bool IsPassThru(CompanyNameFeed xmlObject)
        {
            ShipmentUpdate shipmentUpdateObject = xmlObject as ShipmentUpdate;
            if (IsOrderNumberSFS(shipmentUpdateObject.OrderShipment.OrderNumber)||
                IsOrderNumberSTS(shipmentUpdateObject.OrderShipment.OrderNumber)||
                IsOrderNumberISP(shipmentUpdateObject.OrderShipment.OrderNumber))
            {
                return false;
            }

            return true;
        }

        public CompanyNameFeed GetXmlObject(FileStream file)
        {
            return serializer.Deserialize<ShipmentUpdate>(file);
        }

        public void ManipulateAction(CompanyNameFeed xmlObject, FileStream file)
        {
            ShipmentUpdate shipmentUpdateObject = xmlObject as ShipmentUpdate;
            if (IsOrderNumberSFS(shipmentUpdateObject.OrderShipment.OrderNumber))
            {
                ManipulateSFSShipmentUpdate(shipmentUpdateObject, file);
            }
            else if (IsOrderNumberSTS(shipmentUpdateObject.OrderShipment.OrderNumber))
            {
                ManipulateSTSShipmentUpdate(shipmentUpdateObject, file);
            }
            else if (IsOrderNumberISP(shipmentUpdateObject.OrderShipment.OrderNumber))
            {
                ManipulateISPShipmentUpdate(shipmentUpdateObject, file);
            }
            ArchiveOriginalFile(file);
        }

        public bool ShouldManipulateFile(CompanyNameFeed xmlObject)
        {
            ShipmentUpdate shipmentUpdateObject = xmlObject as ShipmentUpdate;

            if (IsOrderNumberSFS(shipmentUpdateObject.OrderShipment.OrderNumber)||
                IsOrderNumberSTS(shipmentUpdateObject.OrderShipment.OrderNumber)||
                IsOrderNumberISP(shipmentUpdateObject.OrderShipment.OrderNumber))
            {
                return true;
            }
            return false;
        }

        private bool IsOrderNumberSTS(string shipmentUpdateOrderNumber)
        {
            Regex r = new Regex(orderNumberSTSRegEx);
            if (r.Match(shipmentUpdateOrderNumber).Success)
            {
                return true;
            }
            return false;
        }

        private bool IsOrderNumberISP(string shipmentUpdateOrderNumber)
        {
            Regex r = new Regex(orderNumberISPURegEx);
            if (r.Match(shipmentUpdateOrderNumber).Success)
            {
                return true;
            }
            return false;
        }

        private bool IsOrderNumberSFS(string shipmentUpdateOrderNumber)
        {
            Regex r = new Regex(orderNumberSFSRegEx);
            if (r.Match(shipmentUpdateOrderNumber).Success)
            {
                return true;
            }
            return false;
        }

        private void ManipulateSFSShipmentUpdate(ShipmentUpdate shipmentUpdateObject, FileStream file)
        {
            string SFSOrderNumber = shipmentUpdateObject.OrderShipment.OrderNumber;
            string originalOrderNumber = Regex.Replace(shipmentUpdateObject.OrderShipment.OrderNumber, orderNumberSFSRegEx, "");
            string STSOrderNumber = GetSTSOrderNumber(originalOrderNumber);
            

            shipmentUpdateObject.OrderShipment.OrderNumber = originalOrderNumber;
            shipmentUpdateObject.OrderShipment.ShipmentType = "TRANSIT";

            foreach (ShipmentInfo shipInfo in shipmentUpdateObject.OrderShipment.ShipmentInfo)
            {
                shipInfo.TrackingNumber = SFSOrderNumber;
            }

            string shipmentUpdatetoString = serializer.Serialize<ShipmentUpdate>(shipmentUpdateObject);
            transport.SaveToFile(Path.Combine(outgoingFileLocation, Path.GetFileName(file.Name)), shipmentUpdatetoString);


            //Create the STS Order
            var correspondingOiOutgoingSTSObject = GetCorrespondingOiOutgoingSFSObject(SFSOrderNumber);
            var originalOiIncomingSTSEntity = GetOriginalOiIncomingSTSEntity(originalOrderNumber);

            correspondingOiOutgoingSTSObject.OrderInfo.OrderNumber = STSOrderNumber;
            correspondingOiOutgoingSTSObject.OrderInfo.HeaderInfo.ShipMethCode = "STS";
            correspondingOiOutgoingSTSObject.OrderInfo.HeaderInfo.ShipMethod = "STS";
            correspondingOiOutgoingSTSObject.OrderInfo.HeaderInfo.StoreCode = originalOiIncomingSTSEntity.VendorCode;

            //new nodes added for sts order. initializing them here
            correspondingOiOutgoingSTSObject.OrderInfo.HeaderInfo.BeginningStatus = "08";
            correspondingOiOutgoingSTSObject.OrderInfo.HeaderInfo.OrderShipmentInfo = new OrderShipmentInfo();
            correspondingOiOutgoingSTSObject.OrderInfo.HeaderInfo.OrderShipmentInfo.ShipmentMethod = "02";
            correspondingOiOutgoingSTSObject.OrderInfo.HeaderInfo.OrderShipmentInfo.Carrier = "REPLENISHMENT";
            correspondingOiOutgoingSTSObject.OrderInfo.HeaderInfo.OrderShipmentInfo.TrackingNumber = SFSOrderNumber;
            correspondingOiOutgoingSTSObject.OrderInfo.HeaderInfo.OrderShipmentInfo.TrackingURL = @"https:\\" + SFSOrderNumber;


            string writeFileName = STSOrderNumber + ".xml";
            string newSTSOrderString = serializer.Serialize<ImportOrders>(correspondingOiOutgoingSTSObject);
            transport.SaveToFile(Path.Combine(outgoingOiFileLocation, writeFileName), newSTSOrderString);
            
            using (var persistencyService = new PersistencyService())
            {
                persistencyService.RegisterOutgoingSTSOrderImport(SFSOrderNumber, STSOrderNumber, writeFileName, processId);
            }
        }

        private void ManipulateSTSShipmentUpdate(ShipmentUpdate shipmentUpdateObject, FileStream file)
        {
            shipmentUpdateObject.OrderShipment.OrderNumber = Regex.Replace(shipmentUpdateObject.OrderShipment.OrderNumber, orderNumberSTSRegEx, "");          

            string shipmentUpdatetoString = serializer.Serialize<ShipmentUpdate>(shipmentUpdateObject);
            transport.SaveToFile(Path.Combine(outgoingFileLocation, Path.GetFileName(file.Name)), shipmentUpdatetoString);
        }

        private void ManipulateISPShipmentUpdate(ShipmentUpdate shipmentUpdateObject, FileStream file)
        {
            shipmentUpdateObject.OrderShipment.OrderNumber = Regex.Replace(shipmentUpdateObject.OrderShipment.OrderNumber, orderNumberISPURegEx, "");

            string shipmentUpdatetoString = serializer.Serialize<ShipmentUpdate>(shipmentUpdateObject);
            transport.SaveToFile(Path.Combine(outgoingFileLocation, Path.GetFileName(file.Name)), shipmentUpdatetoString);
        }

        private string GetSTSOrderNumber(string originalOrderNumber)
        {
            int OutgoingSTSOrderCount = 0;
            using (var persistencyService = new PersistencyService())
            {
                OutgoingSTSOrderCount = persistencyService.GetOutgoingSFSOrderCount(originalOrderNumber);
            }
            OutgoingSTSOrderCount++; //The counter will always start from 1

            return "ST" + OutgoingSTSOrderCount + "_" + originalOrderNumber;
        }

        private OiIncomingSTS GetOriginalOiIncomingSTSEntity(string originalOrderNumber)
        {
            using (var persistency = new PersistencyService())
            {
                return persistency.GetOiIncomingSTSDetails(originalOrderNumber);
            }
        }

        private ImportOrders GetCorrespondingOiOutgoingSFSObject(string orderNumber)
        {
            using (var persistency = new PersistencyService())
            {
                var correspondingSFSOrder = persistency.GetOiOutgoingSFSDetails(orderNumber);
                return serializer.Deserialize<ImportOrders>(correspondingSFSOrder.FileContents);

            }
        }
    }
}
