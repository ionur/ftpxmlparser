﻿using System.Collections.Generic;
using System.Linq;
using System.IO;
using Companyname.CNCRouter.DataLayer;
using Companyname.CNCRouter.Feeds;
using Companyname.TransportLayer;
using Companyname.CNCRouter.BusinessLayer.Builders;

namespace Companyname.CNCRouter.BusinessLayer.Processors
{
    class OrderImportProcessor : BaseProcessor, IProcessorInterface
    {

        private string originalOrderNumber;
        private List<LineItemInfo> allLineItems;
        private List<LineItemInfo> availableAtStoreItems;
        private List<LineItemInfo> availableAtWarehouseItems;
        private OrderImportBuilder oiBuilder;

        public OrderImportProcessor(
            TransportService transport,
            FeedSerializer serializer,
            string outgoingFileLocation,
            string archiveFileLocation,
            string processId) : base(transport, serializer, outgoingFileLocation, archiveFileLocation, processId)
        {
            oiBuilder = new OrderImportBuilder();
        }

        public CompanyNameFeed GetXmlObject(FileStream file)
        {
            return serializer.Deserialize<ImportOrders>(file);
        }

        public bool IsPassThru(CompanyNameFeed xmlObject)
        {
            ImportOrders importOrdersObject = xmlObject as ImportOrders;
            return importOrdersObject.OrderInfo.HeaderInfo.ShipMethCode == "STANDARD";
        }

        public bool ShouldManipulateFile(CompanyNameFeed xmlObject)
        {
            ImportOrders importOrdersObject = xmlObject as ImportOrders;
            return importOrdersObject.OrderInfo.HeaderInfo.ShipMethCode == "STS";
        }

        public void ManipulateAction(CompanyNameFeed xmlObject, FileStream file)
        {
            ImportOrders importOrdersObject = xmlObject as ImportOrders;
            originalOrderNumber = importOrdersObject.OrderInfo.OrderNumber;
            string storeCode = importOrdersObject.OrderInfo.HeaderInfo.StoreCode;

            ImportOrders sfsOrder = null;
            ImportOrders ispuOrder = null;

            allLineItems = importOrdersObject.OrderInfo.LineItemInfo;
            AvailabilityChecker availabilityChecker = new AvailabilityChecker(
                allLineItems,
                storeCode,
                GetWarehouseCodeFromCountry(importOrdersObject.OrderInfo.HeaderInfo.ContactInfo[1].AddressInfo.CountryCode)
                );

            availableAtStoreItems = availabilityChecker.GetAvailableItemsInStore();
            availableAtWarehouseItems = availabilityChecker.GetAvailableItemsInWarehouse();
            List<LineItemInfo> combinedAvailability = availabilityChecker.GetCombinedAvailability(availableAtStoreItems, availableAtWarehouseItems);

            if (allLineItems.Count == availableAtStoreItems.Count) //all items are available at target store
            {
                //Create a full ISPU order
                ispuOrder = oiBuilder.BuildISPUOrderForItems(importOrdersObject, availableAtStoreItems, storeCode);
            }
            else if (allLineItems.Count == availableAtWarehouseItems.Count) //all items are available at warehouse
            {
                sfsOrder = oiBuilder.BuildSFSOrderForItems(importOrdersObject, availableAtWarehouseItems);
            }
            else if (allLineItems.Count == combinedAvailability.Count) //all items are available either at target store or warehouse
            {
                //Create an ISPU order
                ispuOrder = oiBuilder.BuildISPUOrderForItems(importOrdersObject, availableAtStoreItems, storeCode);
                //Create a Warehouse SFS Order for the items that are *NOT* available at Store 
                sfsOrder = oiBuilder.BuildSFSOrderForItems(importOrdersObject, availableAtWarehouseItems.Intersect(availableAtStoreItems).ToList());
            }

            //Persist Incoming STS Order
            using (var persistencyService = new PersistencyService())
            {
                var persistedSTSOrder = persistencyService.RegisterIncomingSTSOrderImport(
                    originalOrderNumber, 
                    storeCode,
                    Path.GetFileName(file.Name), 
                    File.ReadAllText(file.Name),
                    processId);
            }

            if (ispuOrder != null)
            {
                //Write ISPU Order to file & Persist It
                TransportAndPersistOutgoingOrder(ispuOrder, "ISPU", originalOrderNumber);
            }
            if (sfsOrder != null)
            {
                //Write SFS Order to file & Persist It
                TransportAndPersistOutgoingOrder(sfsOrder, "SFS", originalOrderNumber);
            }
            
            ArchiveOriginalFile(file);
        }
    }
}
