CNC Router / STS Enabler
========================

Middlerware code to enable STS/ISPU for VendorNet.

This middleware will listen and modify/passthru the following feeds for VendorNet --

* New Order Feed
* Shipment Update Feed
* Cancel Update Feed