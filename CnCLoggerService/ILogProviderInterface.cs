﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Companyname.LoggerLayer
{
    public interface ILogProviderInterface
    {
        void Info(string message);

        void Error(string message);

        void Warn(string message);

        void Log(string message);
    }
}
