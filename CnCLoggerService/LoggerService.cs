﻿using NLog;
using NLog.Fluent;

namespace Companyname.LoggerLayer
{
    public class LoggerService
    {
        //provider
        private ILogger logger;
        private string processId;

        public LoggerService(string processId, string fileLoggerFilePath = null)
        {
            this.processId = processId;

            if (!string.IsNullOrEmpty(fileLoggerFilePath))
            {
                var config = LogManager.Configuration;
                var fileTarget = config.FindTargetByName("file") as NLog.Targets.FileTarget;
                fileTarget.FileName = fileLoggerFilePath;
            }

            this.logger = LogManager.GetCurrentClassLogger();
        }

        public void Error(string message)
        {
            logger.Error(messageWithProcessId(message));
        }

        public void Info(string message)
        {
            logger.Info(messageWithProcessId(message));
        }

        public void Log(string message)
        {
            logger.Info(messageWithProcessId(message));
        }

        public void Warn(string message)
        {
            logger.Warn(messageWithProcessId(message));
        }

        private string messageWithProcessId(string message)
        {
            return string.Format("{0}: {1}", processId, message);
        }
    }
}
