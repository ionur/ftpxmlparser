﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Companyname.CNCRouter.DataLayer.Models
{
    public class OiIncomingSTS
    {
        public OiIncomingSTS()
        {
            CreatedAt = DateTime.UtcNow;
        }

        public int OiIncomingSTSId { get; set; }

        [StringLength(30)]
        [Required]
        [Index(IsUnique=true)]
        public string OrderNumber { get; set; }

        [StringLength(30)]
        public string VendorCode { get; set; }

        [StringLength(100)]
        [Required]
        public string FileName { get; set; }

        [Required]
        public string FileContents { get; set; }

        [Column(TypeName="DateTime2")]
        public DateTime CreatedAt { get; set; }

        [Required]
        public string ProcessId { get; set; }

        public virtual ICollection<OiOutgoingSFS> OutgoingSFS { get; set; } 
    }

    public class OiOutgoingISPU
    {
        public OiOutgoingISPU()
        {
            CreatedAt = DateTime.UtcNow;
        }

        public int OiOutgoingISPUId { get; set; }

        [StringLength(30)]
        [Required]
        [Index(IsUnique = true)]
        public string OrderNumber { get; set; }

        [StringLength(100)]
        [Required]
        public string FileName { get; set; }

        [Required]
        public string FileContents { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime CreatedAt { get; set; }

        [ForeignKey("IncomingSTS")]
        public int IncomingSTSId { get; set; }

        [Required]
        public string ProcessId { get; set; }

        public OiIncomingSTS IncomingSTS { get; set; }
    }

    public class OiOutgoingSFS
    {
        public OiOutgoingSFS()
        {
            CreatedAt = DateTime.UtcNow;
        }

        public int OiOutgoingSFSId { get; set; }

        [StringLength(30)]
        [Required]
        [Index(IsUnique=true)]
        public string OrderNumber { get; set; }

        [StringLength(100)]
        [Required]
        public string FileName { get; set; }

        [Required]
        public string FileContents { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime CreatedAt { get; set; }

        [ForeignKey("IncomingSTS")]
        public int IncomingSTSId { get; set; }

        [Required]
        public string ProcessId { get; set; }

        public OiIncomingSTS IncomingSTS { get; set; }
    }

    public class OiOutgoingSTS
    {
        public OiOutgoingSTS()
        {
            CreatedAt = DateTime.UtcNow;
        }

        public int OiOutgoingSTSId { get; set; }

        [StringLength(30)]
        [Required]
        [Index(IsUnique = true)]
        public string OrderNumber { get; set; }

        [StringLength(100)]
        [Required]
        public string FileName { get; set; }

        [Column(TypeName = "DateTime2")]
        public DateTime CreatedAt { get; set; }

        [ForeignKey("OriginalSFSOrder")]
        public int OriginalSFSId { get; set; }

        [Required]
        public string ProcessId { get; set; }

        public OiOutgoingSFS OriginalSFSOrder { get; set; }
    }

}
