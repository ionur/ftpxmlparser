﻿using Companyname.CNCRouter.DataLayer.Models;
using System;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;

namespace Companyname.CNCRouter.DataLayer
{
    public class PersistencyService: IDisposable
    {
        private CncRouterContext context;

        public PersistencyService()
        {
            context = new CncRouterContext();
        }

        public OiIncomingSTS RegisterIncomingSTSOrderImport(string orderNumber, string vendorCode, string fileName, string fileContents, string processId)
        {
            OiIncomingSTS incomingFeed = new OiIncomingSTS()
            {
                OrderNumber = orderNumber,
                VendorCode = vendorCode,
                FileName = fileName,
                FileContents = fileContents,
                ProcessId = processId
            };
            context.OiIncomingSTS.Add(incomingFeed);

            return incomingFeed;
        }

        public OiOutgoingSFS RegisterOutgoingSFSOrderImport(string orderNumber, string fileName, string fileContents, string stsOrderNumber, string processId)
        {
            var incomingSTSFeed = context.OiIncomingSTS.Where(o => o.OrderNumber == stsOrderNumber).First();
            OiOutgoingSFS outgoingFeed = new OiOutgoingSFS()
            {
                OrderNumber = orderNumber,
                FileName = fileName,
                IncomingSTS = incomingSTSFeed,
                FileContents = fileContents,
                ProcessId = processId
            };
            context.OiOutgoingSFS.Add(outgoingFeed);

            return outgoingFeed;
        }

        public OiOutgoingISPU RegisterOutgoingISPUOrderImport(string orderNumber, string fileName, string fileContents, string stsOrderNumber, string processId)
        {
            var incomingSTSFeed = context.OiIncomingSTS.Where(o => o.OrderNumber == stsOrderNumber).First();
            OiOutgoingISPU outgoingFeed = new OiOutgoingISPU()
            {
                OrderNumber = orderNumber,
                FileName = fileName,
                IncomingSTS = incomingSTSFeed,
                FileContents = fileContents,
                ProcessId = processId
            };
            context.OiOutgoingISPU.Add(outgoingFeed);

            return outgoingFeed;
        }

        public void RegisterOutgoingSTSOrderImport(string SFSOrderNumber, string STSOrderNumber, string fileName, string processId)
        {
            var SFSOrder = context.OiOutgoingSFS.Where(o => o.OrderNumber == SFSOrderNumber).First();
            OiOutgoingSTS outgoingFeed = new OiOutgoingSTS()
            {
                OrderNumber = STSOrderNumber,
                OriginalSFSOrder = SFSOrder,
                FileName = fileName,
                ProcessId = processId
            };

            context.OiOutgoingSTS.Add(outgoingFeed);
        }

        public void RegisterIncomingSTSOutgoingSFSOrderImport(string orderNumber, string vendorCode, string fileName, string fileContents, string sfsOrderNumber, string sfsFileName, string sfsFileContents, string processId)
        {
            OiIncomingSTS incomingFeed = new OiIncomingSTS()
            {
                OrderNumber = orderNumber,
                VendorCode = vendorCode,
                FileName = fileName,
                FileContents = fileContents,
                ProcessId = processId
            };

            OiOutgoingSFS outgoingFeed = new OiOutgoingSFS()
            {
                OrderNumber = sfsOrderNumber,
                FileName = sfsFileName,
                IncomingSTS = incomingFeed,
                FileContents = sfsFileContents,
                ProcessId = processId
            };

            context.OiIncomingSTS.Add(incomingFeed);
            context.OiOutgoingSFS.Add(outgoingFeed);
        }



        public int GetOutgoingSFSOrderCount(string orderNumber)
        {
            var query = from incomingOrder in context.OiOutgoingSFS
                        where incomingOrder.OrderNumber == orderNumber
                        select incomingOrder;

            int SFSOrderCount = context.OiOutgoingSFS.Local.Count;
            return SFSOrderCount;
        }

        public OiIncomingSTS GetOiIncomingSTSDetails(string orderNumber)
        {
            return context.OiIncomingSTS.Where(o => o.OrderNumber == orderNumber).First();
        }

        public OiOutgoingSFS GetOiOutgoingSFSDetails(string orderNumber)
        {
            return context.OiOutgoingSFS.Where(o => o.OrderNumber == orderNumber).First();
        }

        public OiOutgoingSTS GetOiOutgoingSTSDetails(string orderNumber)
        {
            return context.OiOutgoingSTS.Where(o => o.OrderNumber == orderNumber).First();
        }

        public void Persist()
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
            }
            
        }

        public void Dispose()
        {
            Persist();
            context.Dispose();
        }
    }
}
