﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Companyname.CNCRouter.DataLayer.Models;

namespace Companyname.CNCRouter.DataLayer
{
    public class CncRouterContext: DbContext
    {
        public CncRouterContext(): base("name=DefaultConnectionString")
        {

        }

        public DbSet<OiIncomingSTS> OiIncomingSTS { get; set; }
        public DbSet<OiOutgoingISPU> OiOutgoingISPU { get; set; }
        public DbSet<OiOutgoingSFS> OiOutgoingSFS { get; set; }
        public DbSet<OiOutgoingSTS> OiOutgoingSTS { get; set; }
    }
}
