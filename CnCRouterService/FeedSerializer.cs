﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Companyname.CNCRouter
{
    public class FeedSerializer
    {
        public T Deserialize<T>(FileStream fileStream)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                XmlReader reader = XmlReader.Create(fileStream);

                T obj;
                obj = (T)serializer.Deserialize(reader);
                fileStream.Close();
                return obj;
            }
            catch
            {
                throw;
            }
        }

        public T Deserialize<T>(string fileContents)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            XmlReader reader = XmlReader.Create(new StringReader(fileContents));

            T obj;
            obj = (T)serializer.Deserialize(reader);
            return obj;
        }


        public string Serialize<T>(T dataToSerialize)
        {
            try
            {
                Utf8StringWriter stringwriter = new Utf8StringWriter();
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(stringwriter, dataToSerialize);
                return stringwriter.ToString();
            }
            catch
            {
                throw;
            }
        }

    }

    public sealed class Utf8StringWriter : StringWriter
    {
        public override Encoding Encoding { get { return Encoding.UTF8; } }
    }
}
