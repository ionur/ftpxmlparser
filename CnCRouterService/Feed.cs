﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Companyname.CNCRouter.Feeds
{
    #region OrderImport 

    [XmlRoot(ElementName = "AddressInfo")]
    public class AddressInfo
    {
        [XmlElement(ElementName = "Street")]
        public string Street { get; set; }

        [XmlElement(ElementName = "Address1")]
        public string Address1 { get; set; }

        [XmlElement(ElementName = "Address2")]
        public string Address2 { get; set; }

        [XmlElement(ElementName = "City")]
        public string City { get; set; }

        [XmlElement(ElementName = "State")]
        public string State { get; set; }

        [XmlElement(ElementName = "PostalCode")]
        public string PostalCode { get; set; }

        [XmlElement(ElementName = "CountryCode")]
        public string CountryCode { get; set; }
    }

    [XmlRoot(ElementName = "ContactInfo")]
    public class ContactInfo
    {
        [XmlElement(ElementName = "ContactType")]
        public string ContactType { get; set; }

        [XmlElement(ElementName = "FirstName")]
        public string FirstName { get; set; }

        [XmlElement(ElementName = "LastName")]
        public string LastName { get; set; }

        [XmlElement(ElementName = "EmailAddress")]
        public string EmailAddress { get; set; }

        [XmlElement(ElementName = "PhoneNumber1")]
        public string PhoneNumber1 { get; set; }

        [XmlElement(ElementName = "PhoneNumber2")]
        public string PhoneNumber2 { get; set; }

        [XmlElement(ElementName = "PhoneNumber3")]
        public string PhoneNumber3 { get; set; }

        [XmlElement(ElementName = "AddressInfo")]
        public AddressInfo AddressInfo { get; set; }
    }

    [XmlRoot(ElementName = "HeaderInfo")]
    public class HeaderInfo
    {
        [XmlElement(ElementName = "BeginningStatus")]
        public string BeginningStatus { get; set; }

        [XmlElement(ElementName = "ShipmentInfo")]
        public OrderShipmentInfo OrderShipmentInfo { get; set; }

        [XmlElement(ElementName = "OrderDate")]
        public string OrderDate { get; set; }

        [XmlElement(ElementName = "ShipMethCode")]
        public string ShipMethCode { get; set; }

        [XmlElement(ElementName = "ShipMethod")]
        public string ShipMethod { get; set; }

        [XmlElement(ElementName = "StoreCode")]
        public string StoreCode { get; set; }

        [XmlElement(ElementName = "AllocStoreCode")]
        public string AllocStoreCode { get; set; }

        [XmlElement(ElementName = "EventId")]
        public string EventId { get; set; }

        [XmlElement(ElementName = "PH")]
        public string PH { get; set; }

        [XmlElement(ElementName = "Tax")]
        public string Tax { get; set; }

        [XmlElement(ElementName = "CompanyCode")]
        public string CompanyCode { get; set; }

        [XmlElement(ElementName = "DivisionCode")]
        public string DivisionCode { get; set; }

        [XmlElement(ElementName = "DeptCode")]
        public string DeptCode { get; set; }

        [XmlElement(ElementName = "EntryOperator")]
        public string EntryOperator { get; set; }

        [XmlElement(ElementName = "ContactInfo")]
        public List<ContactInfo> ContactInfo { get; set; }
    }

    [XmlRoot(ElementName = "ShipmentInfo")]
    public class OrderShipmentInfo
    {
        [XmlElement(ElementName = "ShipmentMethod")]
        public string ShipmentMethod { get; set; }

        [XmlElement(ElementName = "TrackingNumber")]
        public string TrackingNumber { get; set; }

        [XmlElement(ElementName = "Carrier")]
        public string Carrier { get; set; }

        [XmlElement(ElementName = "TrackingURL")]
        public string TrackingURL { get; set; }
    }

    [XmlRoot(ElementName = "LineItemInfo")]
    public class LineItemInfo
    {
        [XmlElement(ElementName = "LineNumber")]
        public string LineNumber { get; set; }

        [XmlElement(ElementName = "SKU")]
        public string SKU { get; set; }

        [XmlElement(ElementName = "StoreSKU")]
        public string StoreSKU { get; set; }

        [XmlElement(ElementName = "Quantity")]
        public string Quantity { get; set; }

        [XmlElement(ElementName = "UnitPrice")]
        public string UnitPrice { get; set; }
    }

    [XmlRoot(ElementName = "OrderInfo")]
    public class OrderInfo
    {
        [XmlElement(ElementName = "OrderNumber")]
        public string OrderNumber { get; set; }

        [XmlElement(ElementName = "HeaderInfo")]
        public HeaderInfo HeaderInfo { get; set; }

        [XmlElement(ElementName = "LineItemInfo")]
        public List<LineItemInfo> LineItemInfo { get; set; }
    }

    [XmlRoot(ElementName = "ImportOrders")]
    public class ImportOrders : CompanyNameFeed, IImportOrders 
    {
        public readonly string FeedIdentifier = "OrderImport";

        [XmlElement(ElementName = "OrderInfo")]
        public OrderInfo OrderInfo { get; set; }

        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string Xsi { get; set; }

        [XmlAttribute(AttributeName = "noNamespaceSchemaLocation", Namespace = "ImportOrders.xsd")]
        public string NoNamespaceSchemaLocation { get; set; }
    }

    #endregion

    
    #region CancelUpdate

    [XmlRoot(ElementName = "CancelInfo")]
    public class CancelInfo
    {
        [XmlElement(ElementName = "CancelAllProducts")]
        public string CancelAllProducts { get; set; }

        [XmlElement(ElementName = "ReasonCode")]
        public string ReasonCode { get; set; }

        [XmlElement(ElementName = "CancelDate")]
        public string CancelDate { get; set; }

        [XmlElement(ElementName = "CancelTime")]
        public string CancelTime { get; set; }

        [XmlElement(ElementName = "ShipMethCode")]
        public string ShipMethCode { get; set; }
    }

    [XmlRoot(ElementName = "CancelProduct")]
    public class CancelProduct
    {
        [XmlElement(ElementName = "LineNumber")]
        public string LineNumber { get; set; }

        [XmlElement(ElementName = "SKU")]
        public string SKU { get; set; }

        [XmlElement(ElementName = "VendorSKU")]
        public string VendorSKU { get; set; }

        [XmlElement(ElementName = "ExternalID")]
        public string ExternalID { get; set; }

        [XmlElement(ElementName = "UPCCode")]
        public string UPCCode { get; set; }

        [XmlElement(ElementName = "CancelledQuantity")]
        public string CancelledQuantity { get; set; }
    }

    [XmlRoot(ElementName = "Header")]
    public class Header
    {
        [XmlElement(ElementName = "VendorNumber")]
        public string VendorNumber { get; set; }

        [XmlElement(ElementName = "OrderNumber")]
        public string OrderNumber { get; set; }

        [XmlElement(ElementName = "HostOrderNumber")]
        public string HostOrderNumber { get; set; }

        [XmlElement(ElementName = "OrderType")]
        public string OrderType { get; set; }

        [XmlElement(ElementName = "Company")]
        public string Company { get; set; }

        [XmlElement(ElementName = "Division")]
        public string Division { get; set; }

        [XmlElement(ElementName = "EntryOperator")]
        public string EntryOperator { get; set; }
    }

    [XmlRoot(ElementName = "CustomerService")]
    public class CustomerService
    {
        [XmlElement(ElementName = "Header")]
        public Header Header { get; set; }

        [XmlElement(ElementName = "CancelInfo")]
        public CancelInfo CancelInfo { get; set; }

        [XmlElement(ElementName = "CancelProduct")]
        public List<CancelProduct> CancelProduct { get; set; }
    }

    [XmlRoot(ElementName = "CustomerServiceUpdate")]
    public class CustomerServiceUpdate : CompanyNameFeed
    {
        public readonly string FeedIdentifier = "CancelUpdate";

        [XmlElement(ElementName = "CustomerService")]
        public CustomerService CustomerService { get; set; }
    }


    #endregion


    #region ShipmentUpdate
    [XmlRoot(ElementName = "VendorInfo")]
    public class VendorInfo
    {
        [XmlElement(ElementName = "VendorNumber")]
        public string VendorNumber { get; set; }

        [XmlElement(ElementName = "VendorName")]
        public string VendorName { get; set; }

        [XmlElement(ElementName = "Address1")]
        public string Address1 { get; set; }

        [XmlElement(ElementName = "City")]
        public string City { get; set; }

        [XmlElement(ElementName = "PostalCode")]
        public string PostalCode { get; set; }

        [XmlElement(ElementName = "CountryCode")]
        public string CountryCode { get; set; }

        [XmlElement(ElementName = "Country")]
        public string Country { get; set; }
    }

    [XmlRoot(ElementName = "ShipmentInfo")]
    public class ShipmentInfo
    {
        [XmlElement(ElementName = "TrackingNumber")]
        public string TrackingNumber { get; set; }

        [XmlElement(ElementName = "Freight")]
        public string Freight { get; set; }

        [XmlElement(ElementName = "Weight")]
        public string Weight { get; set; }

        [XmlElement(ElementName = "ShipmentDate")]
        public string ShipmentDate { get; set; }

        [XmlElement(ElementName = "ShipmentTime")]
        public string ShipmentTime { get; set; }

        [XmlElement(ElementName = "ShipmentMethod")]
        public string ShipmentMethod { get; set; }

        [XmlElement(ElementName = "DestinationPostalCode")]
        public string DestinationPostalCode { get; set; }
    }

    [XmlRoot(ElementName = "InvoiceInfo")]
    public class InvoiceInfo
    {
        [XmlElement(ElementName = "InvoiceTotal")]
        public string InvoiceTotal { get; set; }

        [XmlElement(ElementName = "ProductTotal")]
        public string ProductTotal { get; set; }

        [XmlElement(ElementName = "Terms")]
        public string Terms { get; set; }

        [XmlElement(ElementName = "TermsDays")]
        public string TermsDays { get; set; }

        [XmlElement(ElementName = "TermsPercent")]
        public string TermsPercent { get; set; }

        [XmlElement(ElementName = "FreightAmount")]
        public string FreightAmount { get; set; }

        [XmlElement(ElementName = "HandlingFee")]
        public string HandlingFee { get; set; }

        [XmlElement(ElementName = "TaxAmount")]
        public string TaxAmount { get; set; }

        [XmlElement(ElementName = "DutyAmount")]
        public string DutyAmount { get; set; }

        [XmlElement(ElementName = "OtherCost")]
        public string OtherCost { get; set; }
    }

    [XmlRoot(ElementName = "ProductInfo")]
    public class ProductInfo
    {
        [XmlElement(ElementName = "LineNumber")]
        public string LineNumber { get; set; }

        [XmlElement(ElementName = "SKU")]
        public string SKU { get; set; }

        [XmlElement(ElementName = "VendorSKU")]
        public string VendorSKU { get; set; }

        [XmlElement(ElementName = "ExternalID")]
        public string ExternalID { get; set; }

        [XmlElement(ElementName = "UPCCode")]
        public string UPCCode { get; set; }

        [XmlElement(ElementName = "OrderedQuantity")]
        public string OrderedQuantity { get; set; }

        [XmlElement(ElementName = "ShippedQuantity")]
        public string ShippedQuantity { get; set; }

        [XmlElement(ElementName = "Factor")]
        public string Factor { get; set; }

        [XmlElement(ElementName = "InvoicedQuantity")]
        public string InvoicedQuantity { get; set; }

        [XmlElement(ElementName = "ExtendedCost")]
        public string ExtendedCost { get; set; }

        [XmlElement(ElementName = "UnitCost")]
        public string UnitCost { get; set; }

        [XmlElement(ElementName = "StandardFreight")]
        public string StandardFreight { get; set; }
    }

    [XmlRoot(ElementName = "OrderShipment")]
    public class OrderShipment
    {
        [XmlElement(ElementName = "ShipmentType")]
        public string ShipmentType { get; set; }

        [XmlElement(ElementName = "VendorInfo")]
        public VendorInfo VendorInfo { get; set; }

        [XmlElement(ElementName = "OrderNumber")]
        public string OrderNumber { get; set; }

        [XmlElement(ElementName = "HostOrderNumber")]
        public string HostOrderNumber { get; set; }

        [XmlElement(ElementName = "OrderType")]
        public string OrderType { get; set; }

        [XmlElement(ElementName = "Company")]
        public string Company { get; set; }

        [XmlElement(ElementName = "Division")]
        public string Division { get; set; }

        [XmlElement(ElementName = "ShipAllProducts")]
        public string ShipAllProducts { get; set; }

        [XmlElement(ElementName = "EntryOperator")]
        public string EntryOperator { get; set; }

        [XmlElement(ElementName = "GroupNumber")]
        public string GroupNumber { get; set; }

        [XmlElement(ElementName = "ShipmentInfo")]
        public List<ShipmentInfo> ShipmentInfo { get; set; }

        [XmlElement(ElementName = "InvoiceInfo")]
        public InvoiceInfo InvoiceInfo { get; set; }

        [XmlElement(ElementName = "ProductInfo")]
        public List<ProductInfo> ProductInfo { get; set; }

        [XmlElement(ElementName = "UniqueReference")]
        public string UniqueReference { get; set; }        

    }

    [XmlRoot(ElementName = "ShipmentUpdate")]
    public class ShipmentUpdate : CompanyNameFeed
    {
        public readonly string FeedIdentifier = "ShipmentUpdate";

        [XmlElement(ElementName = "OrderShipment")]
        public OrderShipment OrderShipment { get; set; }
    }
    #endregion


    #region Abstract Class
    public abstract class CompanyNameFeed
    {

    }
    #endregion

}

