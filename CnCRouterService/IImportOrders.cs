﻿namespace Companyname.CNCRouter.Feeds
{
    public interface IImportOrders
    {
        string NoNamespaceSchemaLocation { get; set; }
        OrderInfo OrderInfo { get; set; }
        string Xsi { get; set; }
    }
}