﻿using Companyname.CNCRouter.BusinessLayer;
using Companyname.LoggerLayer;
using Companyname.TransportLayer;
using Companyname.TransportLayer.Providers;
using System;
using System.IO;

namespace Companyname.CNCRouter
{
    class Program
    {
        static void Main(string[] args)
        {
            ApplicationSettings settings = new ApplicationSettings();

            string programId = GenerateProgramId();
            LoggerService programLogger = new LoggerService(programId,settings.LoggingFileConfiguration["ProgramLevelLog"]);
            programLogger.Info("Starting PROGRAM");

            var transportProvider = GetTransportProvider(settings);
            TransportService transport = new TransportService(transportProvider);
            FeedSerializer serializer = new FeedSerializer();
            
            foreach (string key in settings.supportedFeedTypes)
            {
                if (Boolean.Parse(settings.supportedFeedTypes[key]))
                {
                    programLogger.Info(string.Format("Begin Processing {0} files", key));
                    programLogger.Info(string.Format("Looking for files in {0}", settings.incomingFileLocations[key]));
                    string[] feedFiles = transport.ListDirectory(settings.incomingFileLocations[key]);

                    if (feedFiles.Length == 0)
                    {
                        programLogger.Info(string.Format("No files to process of type {0}", key));
                        continue;
                    }

                    foreach (string filePath in feedFiles)
                    {
                        string processId = GenerateProcessId();
                       
                        LoggerService processLogger = new LoggerService(processId, settings.LoggingFileConfiguration[key]);

                        processLogger.Log(string.Format("Processing {0}", filePath));
                        FileStream feedFileStream = transport.GetFile(filePath);                        

                        LogicService logic = new LogicService(feedFileStream, transport, serializer, settings, processId);
                        logic.setLogger(processLogger);

                        logic.Process();

                    }

                    programLogger.Info(string.Format("End Processing {0} files", key));

                }
            }
            programLogger.Info("Ending PROGRAM");
        }

        private static string GenerateProcessId()
        {
            return DateTime.UtcNow.ToString("yyyyMMdd|HHmm|ffffff");
        }

        private static string GenerateProgramId()
        {
            return DateTime.UtcNow.ToString("yyyyMMdd|HHmm");
        }

        private static IProviderInterface GetTransportProvider(ApplicationSettings settings)
        {
            switch (settings.TransportLayer["provider"].ToUpper())
            {
                case "SFTP":
                    return new SFTPProvider(settings.SFTPConfiguration["HostName"], 
                                                   settings.SFTPConfiguration["UserName"],
                                                   settings.SFTPConfiguration["Password"],
                                                   Convert.ToInt32(settings.SFTPConfiguration["Port"]),
                                                   settings.SFTPConfiguration["UnlockComponent"],
                                                   settings.SFTPConfiguration["TemporaryLocalDownloadPath"],
                                                   settings.SFTPConfiguration["TemporaryLocalUploadPath"]);
                default:
                    return new FileSystemProvider();
            }
        }
    }

    
}
