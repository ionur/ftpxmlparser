﻿using System;
using System.IO;
using Companyname.TransportLayer;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Companyname.CNCRouter.Feeds;
using Companyname.CNCRouter.DataLayer;

namespace Companyname.CNCRouter
{
    class Logic
    {
        private TransportService transport;
        private FeedSerializer serializer;

        private NameValueCollection incomingFilenameMasks;
        private NameValueCollection outgoingFileLocations;
        private NameValueCollection archiveFileLocations;

        public Logic(TransportService transport,
                     FeedSerializer serializer,
                     NameValueCollection incomingFilenameMasks,
                     NameValueCollection outgoingFileLocations,
                     NameValueCollection archiveFileLocations)
        {
            this.transport = transport;
            this.serializer = serializer;

            this.incomingFilenameMasks = incomingFilenameMasks;
            this.outgoingFileLocations = outgoingFileLocations;
            this.archiveFileLocations = archiveFileLocations;
        }

        public void Process(FileStream file)
        {
            Console.WriteLine(file.Name);
            string feedType = GetFeedType(Path.GetFileName(file.Name));
            CompanyNameFeed xmlObject = GetXmlObject(file, feedType);

            if (IsPassThru(xmlObject))
            {
                PassThruAction(file, feedType);
                return;
            }


            if (ShouldManipulateFile(xmlObject))
            {
                ManipulateAction(xmlObject, file);
                ArchiveOriginalFile(file, feedType);
                return;
            }
        }



        #region Helper functions, serialize, deserialize, get feedtype
        private CompanyNameFeed GetXmlObject(FileStream file, string feedType)
        {
            if (feedType == "OrderImport")
            {
                return serializer.Deserialize<ImportOrders>(file);
            }

            if (feedType == "CancelUpdate")
            {
                return serializer.Deserialize<CustomerServiceUpdate>(file);
            }

            if (feedType == "ShipmentUpdate")
            {
                return serializer.Deserialize<ShipmentUpdate>(file);
            }
            throw new Exception("Object De/Serialization is not supported for " + feedType);
        }

        private string GetFeedType(string filename)
        {
            foreach (string key in incomingFilenameMasks)
            {
                string incomingFilenameMask = incomingFilenameMasks[key];
                Regex r = new Regex(incomingFilenameMask);
                Match m = r.Match(filename);
                if (m.Success)
                {
                    return key;
                }
            }
            throw new Exception("Could not get the feed type of incoming feed");
        }

        private void ArchiveOriginalFile(FileStream file, string feedType)
        {
            string destinationFile = Path.Combine(archiveFileLocations[feedType], Path.GetFileName(file.Name));
            transport.MoveFile(file.Name, destinationFile);
        }


        #endregion


        #region Pass Thru Conditions, US#1 US#4 US#6
        private bool IsPassThru(CompanyNameFeed xmlObject)
        {
            if (xmlObject is ImportOrders)
            {
                ImportOrders importOrderObject = (ImportOrders)xmlObject;
                return IsPassThru(importOrderObject);
            }

            if (xmlObject is ShipmentUpdate)
            {
                ShipmentUpdate shipmentUpdateObject = (ShipmentUpdate)xmlObject;
                return IsPassThru(shipmentUpdateObject);
            }

            if (xmlObject is CustomerServiceUpdate)
            {
                CustomerServiceUpdate customerServiceUpdateObject = (CustomerServiceUpdate)xmlObject;
                return IsPassThru(customerServiceUpdateObject);
            }

            return false;
        }

        private bool IsPassThru(ImportOrders importOrderObject)
        {
            return importOrderObject.OrderInfo.HeaderInfo.ShipMethCode == "STANDARD";
        }

        private bool IsPassThru(ShipmentUpdate shipmentUpdateObject)
        {

            if (shipmentUpdateObject.OrderShipment.OrderNumber.Contains("STS_"))
            {
                return false;
            }

            if (!shipmentUpdateObject.OrderShipment.OrderNumber.Contains("SFS_"))
            {
                return true;
            }

            return false;
        }

        private bool IsPassThru(CustomerServiceUpdate customerServiceUpdateObject)
        {
            return !(customerServiceUpdateObject.CustomerService.CancelInfo.ReasonCode == "ISPD");
        }

        private void PassThruAction(FileStream file, string feedType)
        {
            string destinationFile = Path.Combine(outgoingFileLocations[feedType], Path.GetFileName(file.Name));
            transport.MoveFile(file.Name, destinationFile);
        }
        #endregion


        #region Should Manipulate Conditions
        private bool ShouldManipulateFile(CompanyNameFeed xmlObject)
        {
            if (xmlObject is ImportOrders)
            {
                ImportOrders importOrderObject = (ImportOrders)xmlObject;
                return ShouldManipulateFile(importOrderObject);
            }

            if (xmlObject is ShipmentUpdate)
            {
                ShipmentUpdate shipmentUpdateObject = (ShipmentUpdate)xmlObject;
                return ShouldManipulateFile(shipmentUpdateObject);
            }
            return false;
        }

        private bool ShouldManipulateFile(ImportOrders importOrdersObject)
        {
            return importOrdersObject.OrderInfo.HeaderInfo.ShipMethCode == "STS";
        }

        private bool ShouldManipulateFile(ShipmentUpdate shipmentUpdateObject)
        {
            if (shipmentUpdateObject.OrderShipment.OrderNumber.Contains("SFS_"))
            {
                return true;
            }

            if (shipmentUpdateObject.OrderShipment.OrderNumber.Contains("STS_"))
            {
                return true;
            }

            return false;
        }

        private void ManipulateAction(CompanyNameFeed xmlObject, FileStream file)
        {
            if (xmlObject is ImportOrders)
            {
                ImportOrders importOrderObject = (ImportOrders)xmlObject;
                ManipulateAction(importOrderObject, file);
            }

            if (xmlObject is ShipmentUpdate)
            {
                ShipmentUpdate shipmentUpdateObject = (ShipmentUpdate)xmlObject;
                ManipulateAction(shipmentUpdateObject, file);
            }
        }

        private void ManipulateAction(ImportOrders importOrderObject, FileStream file)
        {
            string originalOrderNumber = importOrderObject.OrderInfo.OrderNumber;
            string newOrderNumber = "SFS_" + importOrderObject.OrderInfo.OrderNumber;
            importOrderObject.OrderInfo.OrderNumber = newOrderNumber;
            importOrderObject.OrderInfo.HeaderInfo.ShipMethCode = "STANDARD";

            string writeFileName = importOrderObject.OrderInfo.OrderNumber.ToString() + ".xml";
            string importOrdertoString = serializer.Serialize<ImportOrders>(importOrderObject);
            transport.SaveToFile(Path.Combine(outgoingFileLocations["OrderImport"], writeFileName), importOrdertoString);

            //using (var persistencyService = new PersistencyService())
            //{
            //    persistencyService.RegisterIncomingSTSOrderImport(
            //        originalOrderNumber,
            //        "",
            //        file.Name,
            //        "",
            //        newOrderNumber,
            //        writeFileName,
            //        ""
            //        );
            //}
        }

        private void ManipulateAction(ShipmentUpdate shipmentUpdateObject, FileStream file)
        {
            if (shipmentUpdateObject.OrderShipment.OrderNumber.Contains("SFS_"))
            {
                ShipmentUpdateSFSOrderManipulator(shipmentUpdateObject, file);
                return;
            }

            if (shipmentUpdateObject.OrderShipment.OrderNumber.Contains("STS_"))
            {
                ShipmentUpdateSTSOrderManipulator(shipmentUpdateObject, file);
                return;
            }

        }

        private void ShipmentUpdateSFSOrderManipulator(ShipmentUpdate shipmentUpdateObject, FileStream file)
        {
            string orderNumberChanger = "SFS_";
            shipmentUpdateObject.OrderShipment.OrderNumber = shipmentUpdateObject.OrderShipment.OrderNumber.Replace(orderNumberChanger, "");
            shipmentUpdateObject.OrderShipment.ShipmentType = "TRANSIT";

            foreach (ShipmentInfo shipInfo in shipmentUpdateObject.OrderShipment.ShipmentInfo)
            {
                shipInfo.TrackingNumber = orderNumberChanger + shipmentUpdateObject.OrderShipment.OrderNumber;
            }

            string shipmentUpdatetoString = serializer.Serialize<ShipmentUpdate>(shipmentUpdateObject);
            transport.SaveToFile(Path.Combine(outgoingFileLocations["ShipmentUpdate"], Path.GetFileName(file.Name)), shipmentUpdatetoString);
        }

        private void ShipmentUpdateSTSOrderManipulator(ShipmentUpdate shipmentUpdateObject, FileStream file)
        {
            string orderNumberChanger = "STS_";
            shipmentUpdateObject.OrderShipment.OrderNumber = shipmentUpdateObject.OrderShipment.OrderNumber.Replace(orderNumberChanger, "");

            string shipmentUpdatetoString = serializer.Serialize<ShipmentUpdate>(shipmentUpdateObject);
            transport.SaveToFile(Path.Combine(outgoingFileLocations["ShipmentUpdate"], Path.GetFileName(file.Name)), shipmentUpdatetoString);
        }
        #endregion
    }
}
